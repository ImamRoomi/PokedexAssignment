using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
public class DataFromJson : MonoBehaviour
{
    [SerializeField] private string id;

    [SerializeField] private Image im;
    private Texture2D myTexture;

    // Start is called before the first frame update
    void Start()
    {
        getdata();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void getdata()
    {
        Debug.LogError("hello");
        StartCoroutine(GetText());

    }

    public void OnClicked()
    {
        int t = int.Parse(id);

        pokemoninfo.info(t,myTexture);
    }
    
    
    
    
    IEnumerator GetText()
    {
        using (UnityWebRequest uwr = UnityWebRequestTexture.GetTexture("https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/home/"+id+".png"))
        {
            yield return uwr.SendWebRequest();

            if (uwr.result != UnityWebRequest.Result.Success)
            {
                Debug.Log(uwr.error);
            }
            else
            {
                
                
                
                
                
                // Get downloaded asset bundle
                 myTexture = DownloadHandlerTexture.GetContent(uwr);               
                
                im.sprite = Sprite.Create(myTexture, new Rect(0, 0, myTexture.width, myTexture.height), new Vector2(0.5f, 0.5f));
               


            }
        }


    }
    
    
}
