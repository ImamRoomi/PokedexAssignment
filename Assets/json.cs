using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class json : MonoBehaviour
{
    public Root pokdata;
    // Start is called before the first frame update
    public string jsonString;
    [SerializeField] private TextAsset jsonData;

    void Start()
    { 
        jsonString = jsonData.text;
        pokdata = JsonUtility.FromJson<Root>(jsonString);
    }
    
}

[Serializable]
public class PokemonDataa
{
    public int id ;
    public string name ;
    public int height ;
    public int weight ;
    public string spriteURL ;
    public List<string> abilities ;
    public string type ;
}

[Serializable]
public class Root
{
    public List<PokemonDataa> pokemonDatas ;

}
