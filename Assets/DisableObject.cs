using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisableObject : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] private GameObject Canvas1;
    [SerializeField] private GameObject canvas2;
    private bool c = false;
    void Start()
    {
        
    }

    public void Disable()
    {
        if (c == false)
        {
            Canvas1.SetActive(false);
            canvas2.SetActive(false);
            c = true;

        }
        else
        {
            c = false;

            Canvas1.SetActive(true);
            canvas2.SetActive(true);
        }

    }
    
    
    // Update is called once per frame
    void Update()
    {
        
    }
}
