using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataFormat : MonoBehaviour
{
    public List<PokemonData> pokemonDatas { get; set; }
}
// Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse);
public class PokemonData
{
    public int id { get; set; }
    public string name { get; set; }
    public int height { get; set; }
    public int weight { get; set; }
    public string spriteURL { get; set; }
    public List<string> abilities { get; set; }
    public string type { get; set; }
}