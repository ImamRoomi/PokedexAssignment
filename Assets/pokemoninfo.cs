using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class pokemoninfo : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] private json js;
    private int totalnumberofpaokemon = 35;
    public static pokemoninfo instance;

    [SerializeField] private Image im;
    [SerializeField] private Text name;
    [SerializeField] private Text weight;
    [SerializeField] private Text height;
    [SerializeField] private Text type;
    void Start()
    {
        
    }

    private void Awake()
    {
        instance = this;
    }


   public  void Compare_info(int idno,Texture2D tex)
    {
        for (int a = 0; a < totalnumberofpaokemon; a++)
        {
            if (js.pokdata.pokemonDatas[a].id == idno)
            {
                im.sprite = Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), new Vector2(0.5f, 0.5f));
                name.text = js.pokdata.pokemonDatas[a].name;
                weight.text = js.pokdata.pokemonDatas[a].weight.ToString();
                height.text = js.pokdata.pokemonDatas[a].height.ToString();
                type.text = js.pokdata.pokemonDatas[a].type.ToString();

                Debug.LogError(js.pokdata.pokemonDatas[a].height);
                Debug.LogError(js.pokdata.pokemonDatas[a].weight);
                Debug.LogError(js.pokdata.pokemonDatas[a].name);
                Debug.LogError(js.pokdata.pokemonDatas[a].type);
            }
            
        }
        
    }

   public static void info(int id,Texture2D tex)
   {
       pokemoninfo.instance.Compare_info(id,tex);
   }
    // Update is called once per frame
    void Update()
    {
        
    }
}
